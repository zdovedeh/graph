//
// Created by zdove on 11.02.2019.
//

//#include <algorithm>
#include "Graph.h"
#include "AdjacencyMatrix.h"
#include "AdjacencyList.h"
#include "ListOfEdges.h"

using std::ios;

Graph::Graph() {
    this->graph = new AdjacencyMatrix();
}

Graph::Graph(int N, bool isDirected, bool isWeighted) {
    this->graph = new AdjacencyMatrix(N, isDirected, isWeighted);
}

Graph::~Graph() {
    delete this->graph;
}

void Graph::readGraph(string fileName) {
    delete this->graph; // delete after read
    fstream fin;
    fin.open(fileName, ios::in);
    char view;
    fin >> view;
    switch (view) {
        case 'C':
            this->graph = new AdjacencyMatrix();
            break;
        case 'L':
            this->graph = new AdjacencyList();
            break;
        case 'E':
            this->graph = new ListOfEdges();
            break;
        default:;
    }
    fin.close();
    this->graph->readGraph(fileName);
}

void Graph::addEdge(int from, int to, int weight) {
    this->graph->addEdge(from, to, weight);
}

void Graph::removeEdge(int from, int to) {
    this->graph->removeEdge(from, to);
}

int Graph::changeEdge(int from, int to, int newWeight) {
    return this->graph->changeEdge(from, to, newWeight);
}

void Graph::writeGraph(string fileName) const {
    /*ofstream fout;
    fout.open(fileName, ios::out);
    fout << "Graph contains " << this->verticesCount << " vertices and " << this->edgesCount << " edges" << endl;
    fout.close();*/
    this->graph->writeGraph(fileName);
}

void Graph::transformToAdjList() {
    //BaseGraph* temp = this->graph;
    this->graph = this->graph->transformToAdjacencyList();
    //delete temp;
}

void Graph::transformToAdjMatrix() {
    //BaseGraph* temp = this->graph;
    this->graph = this->graph->transformToAdjacencyMatrix();
    //delete temp;
}

void Graph::transformToListOfEdges() {
    //BaseGraph* temp = this->graph;
    this->graph = this->graph->transformToListOfEdges();
    //delete temp;
}

Graph Graph::getSpaingTreePrima() {
    transformToAdjList();
    return Graph(this->graph->getSpaingTreePrima());
}

Graph Graph::getSpaingTreeKruscal() {
    transformToListOfEdges();
    return Graph(this->graph->getSpaingTreeKruscal());
}

Graph::Graph(BaseGraph *graph) {
    this->graph = graph;
}

Graph Graph::getSpaingTreeBoruvka() {
    transformToListOfEdges();
    return Graph(this->graph->getSpaingTreeBoruvka());
}

/*int Graph::checkEuler(bool &circleExist) const {
    circleExist = false;
    return 0;
}*/
