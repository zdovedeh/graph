//
// Created by zdove on 14.02.2019.
//

#include "AdjacencyMatrix.h"
#include "AdjacencyList.h"
#include "ListOfEdges.h"

using std::ios;
using std::endl;

AdjacencyMatrix::AdjacencyMatrix() {
    this->verticesCount = 0;
    this->edgesCount = 0;
    this->directed = false;
    this->weighted = false;
}

AdjacencyMatrix::AdjacencyMatrix(int N, bool isDirected, bool isWeighted) {
    this->verticesCount = N;
    this->edgesCount = 0;
    this->directed = isDirected;
    this->weighted = isWeighted;
    this->adjacencyMatrix = vector<vector<int>>(N, vector<int>(N, INF));
}

AdjacencyMatrix::~AdjacencyMatrix() {
    for (int i = 0; i < this->verticesCount; ++i) {
        this->adjacencyMatrix[i].clear();
    }
    this->adjacencyMatrix.clear();
}

BaseGraph* AdjacencyMatrix::readGraph(string fileName) {
    fstream fin;
    fin.open(fileName, ios::in);
    char view;
    fin >> view >> this->verticesCount;
    fin >> this->directed >> this->weighted;
    this->adjacencyMatrix = vector<vector<int>>(this->verticesCount, vector<int>(this->verticesCount, INF));
    for (int i = 0; i < this->verticesCount; ++i) {
        for (int j = 0; j < this->verticesCount; ++j) {
            fin >> this->adjacencyMatrix[i][j];
            if (this->adjacencyMatrix[i][j] == 0) {
                this->adjacencyMatrix[i][j] = INF;
            }
            if ((this->directed || i < j) && this->adjacencyMatrix[i][j] != INF) {
                this->edgesCount++;
            }
        }
    }
    fin.close();
    return this;
}

void AdjacencyMatrix::addEdge(int from, int to, int weight) {
    if (!this->weighted && weight != 1) {
        return;
    }
    if (this->adjacencyMatrix[from][to] != INF) {
        return;
    }
    this->edgesCount++;
    this->adjacencyMatrix[from][to] = weight;
    if (!this->directed) {
        this->adjacencyMatrix[to][from] = weight;
    }
}

void AdjacencyMatrix::removeEdge(int from, int to) {
    if (this->adjacencyMatrix[from][to] == INF) {
        return;
    }
    this->edgesCount--;
    this->adjacencyMatrix[from][to] = INF;
    if (!this->directed) {
        this->adjacencyMatrix[to][from] = INF;
    }
}

int AdjacencyMatrix::changeEdge(int from, int to, int newWeight) {
    int oldWeight = this->adjacencyMatrix[from][to];
    if (!this->weighted) {
        return oldWeight;
    }
    this->adjacencyMatrix[from][to] = newWeight;
    if (!this->directed) {
        this->adjacencyMatrix[to][from] = newWeight;
    }
    return oldWeight;
}

BaseGraph * AdjacencyMatrix::transformToAdjacencyList() {
    BaseGraph* graph = new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    for (int i = 0; i < this->verticesCount; ++i) {
        for (int j = 0; j < this->verticesCount; ++j) {
            if ((this->directed || i <= j) && this->adjacencyMatrix[i][j] != INF) {
                graph->addEdge(i, j, this->adjacencyMatrix[i][j]);
            }
        }
        this->adjacencyMatrix[i].clear();
    }
    return graph;
}

BaseGraph * AdjacencyMatrix::transformToListOfEdges() {
    BaseGraph* graph = new ListOfEdges(this->verticesCount, this->directed, this->weighted);
    for (int i = 0; i < this->verticesCount; ++i) {
        for (int j = 0; j < this->verticesCount; ++j) {
            if ((this->directed || i <= j) && this->adjacencyMatrix[i][j] != INF) {
                graph->addEdge(i, j, this->adjacencyMatrix[i][j]);
            }
        }
        this->adjacencyMatrix[i].clear();
    }
    return graph;
}

void AdjacencyMatrix::writeGraph(string fileName) const {
    //Graph::writeGraph(fileName);
    fstream fout;
    //fout.open(fileName, ios::out | ios::app);
    fout.open(fileName, ios::out);
    fout << "C " << this->verticesCount << endl;
    fout << this->directed << " " << this->weighted << endl;
    for (int i = 0; i < this->verticesCount; ++i) {
        for (int j = 0; j < this->verticesCount; ++j) {
            if (this->adjacencyMatrix[i][j] != INF) {
                fout << this->adjacencyMatrix[i][j] << " ";
            } else {
                fout << 0 << " ";
            }
        }
        fout << endl;
    }
    fout.close();
}

BaseGraph *AdjacencyMatrix::transformToAdjacencyMatrix() {
    return this;
}

BaseGraph *AdjacencyMatrix::getSpaingTreeKruscal() const {
    return nullptr;
}

BaseGraph *AdjacencyMatrix::getSpaingTreePrima() const {
    return nullptr;
}

BaseGraph *AdjacencyMatrix::getSpaingTreeBoruvka() const {
    return nullptr;
}
