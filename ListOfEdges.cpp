//
// Created by zdove on 14.02.2019.
//

#include <algorithm>
#include "AdjacencyMatrix.h"
#include "AdjacencyList.h"
#include "ListOfEdges.h"
#include "DSU.h"

using std::ios;
using std::endl;
using std::swap;
using std::sort;

ListOfEdges::ListOfEdges() {
    this->verticesCount = 0;
    this->edgesCount = 0;
    this->directed = false;
    this->weighted = false;
}

ListOfEdges::ListOfEdges(int N, bool isDirected, bool isWeighted) {
    this->verticesCount = N;
    this->edgesCount = 0;
    this->directed = isDirected;
    this->weighted = isWeighted;
}

ListOfEdges::~ListOfEdges() {
    this->listOfEdges.clear();
}

BaseGraph *ListOfEdges::readGraph(string fileName) {
    fstream fin;
    fin.open(fileName, ios::in);
    char view;
    int edgesCount;
    fin >> view >> this->verticesCount >> edgesCount;
    fin >> this->directed >> this->weighted;
    for (int i = 0; i < edgesCount; ++i) {
        int from, to, weight;
        fin >> from >> to;
        if (this->weighted) {
            fin >> weight;
        } else {
            weight = 1;
        }
        this->addEdge(--from, --to, weight);
    }
    fin.close();
    return this;
}

void ListOfEdges::addEdge(int from, int to, int weight) {
    if (!this->weighted && weight != 1) {
        return;
    }
    if (!this->directed && from > to) {
        swap(from, to);
    }
    if (this->listOfEdges.insert(pair<pair<int, int>, int>(pair<int, int>(from, to), weight)).second) {
        this->edgesCount++;
    }
}

void ListOfEdges::removeEdge(int from, int to) {
    if (!this->directed && from > to) {
        swap(from, to);
    }
    if (this->listOfEdges.find(pair<int, int>(from, to)) == this->listOfEdges.end()) {
        return;
    }
    this->edgesCount--;
    this->listOfEdges.erase(pair<int, int>(from, to));
}

int ListOfEdges::changeEdge(int from, int to, int newWeight) {
    int oldWeight = this->listOfEdges[pair<int, int>(to, from)];
    if (!this->weighted) {
        return oldWeight;
    }
    if (!this->directed && from > to) {
        swap(from, to);
    }
    this->listOfEdges[pair<int, int>(from, to)] = newWeight;
    return oldWeight;
}

BaseGraph * ListOfEdges::transformToAdjacencyList() {
    BaseGraph* graph = new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    for (auto it = this->listOfEdges.begin(); it != this->listOfEdges.end(); ) {
        graph->addEdge(it->first.first, it->first.second, it->second);
        this->listOfEdges.erase((it++)->first);
    }
    return graph;
}

BaseGraph * ListOfEdges::transformToAdjacencyMatrix() {
    BaseGraph* graph = new AdjacencyMatrix(this->verticesCount, this->directed, this->weighted);
    for (auto it = this->listOfEdges.begin(); it != this->listOfEdges.end(); ) {
        graph->addEdge(it->first.first, it->first.second, it->second);
        this->listOfEdges.erase((it++)->first);
    }
    return graph;
}

void ListOfEdges::writeGraph(string fileName) const {
    //Graph::writeGraph(fileName);
    fstream fout;
    //fout.open(fileName, ios::out | ios::app);
    fout.open(fileName, ios::out);
    fout << "E " << this->verticesCount << " " << this->edgesCount << endl;
    fout << this->directed << " " << this->weighted << endl;
    for (auto &edge : listOfEdges) {
        fout << edge.first.first + 1 << " " << edge.first.second + 1 << " " << edge.second << endl;
    }
    fout.close();
}

BaseGraph *ListOfEdges::transformToListOfEdges() {
    return this;
}

BaseGraph* ListOfEdges::getSpaingTreeKruscal() const {
    if (this->edgesCount == 0)
        return new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    BaseGraph* spaningTree = new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    DSU dsu = DSU(this->verticesCount);
    vector<pair<int, pair<int, int>>> sortEdges;
    sortEdges.reserve(this->edgesCount);
    for (auto edge : listOfEdges) {
        sortEdges.emplace_back(pair<int, pair<int, int>>(edge.second, pair<int, int>(edge.first.first, edge.first.second)));
    }
    sort(sortEdges.begin(), sortEdges.end());
    for (auto edge : sortEdges) {
        if (dsu.find(edge.second.first) != dsu.find(edge.second.second)) {
            dsu.unite(dsu.find(edge.second.first), dsu.find(edge.second.second));
            spaningTree->addEdge(edge.second.first, edge.second.second, edge.first);
        }
    }
    return spaningTree;
}

BaseGraph *ListOfEdges::getSpaingTreeBoruvka() const {
    if (this->edgesCount == 0)
        return new ListOfEdges(this->verticesCount, this->directed, this->weighted);
    BaseGraph* spaningTree = new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    int edgesCount = 0;
    DSU dsu = DSU(this->verticesCount);
    vector<pair<int, pair<int, int>>> minEdges;
    int i, j;
    while (edgesCount < this->verticesCount - 1) {
        minEdges.assign(this->verticesCount, pair<int, pair<int, int>>(INF, pair<int, int>(0, 0)));
        for (auto edge : this->listOfEdges) {
            i = dsu.find(edge.first.first);
            j = dsu.find(edge.first.second);
            if (i != j) {
                if (minEdges[i].first > edge.second) {
                    minEdges[i].first = edge.second;
                    minEdges[i].second = edge.first;
                }
                if (!this->directed && minEdges[j].first > edge.second) {
                    minEdges[j].first = edge.second;
                    minEdges[j].second = edge.first;
                }
            }
        }
        for (auto minEdge : minEdges) {
            if (minEdge.first != INF && dsu.find(minEdge.second.first) != dsu.find(minEdge.second.second)) {
                dsu.unite(dsu.find(minEdge.second.first), dsu.find(minEdge.second.second));
                spaningTree->addEdge(minEdge.second.first, minEdge.second.second, minEdge.first);
                edgesCount++;
            }
        }
    }
    minEdges.clear();
    return spaningTree;
}

BaseGraph *ListOfEdges::getSpaingTreePrima() const {
    return nullptr;
}

/*int ListOfEdges::checkEuler(bool &circleExist) const {
    pair<int, int> beginVertex;
    int oddDegreeVerticesCount = getOddDegreeVerticesCount(beginVertex.first);
    int componentsWithEdgesCount = getComponentsWithEdgesCount(beginVertex.second);
    circleExist = oddDegreeVerticesCount <= 2 && componentsWithEdgesCount <= 1;
    if (oddDegreeVerticesCount == 0 && componentsWithEdgesCount <= 1) {
        circleExist = true;
        return beginVertex.second + 1;
    } else if (oddDegreeVerticesCount <= 2 && componentsWithEdgesCount <= 1) {
        circleExist = false;
        return beginVertex.first + 1;
    } else {
        circleExist = false;
        return 0;
    }
}*/

/*int ListOfEdges::getComponentsWithEdgesCount(int& beginVertex) const {
    DSU dsu = DSU(verticesCount);
    vector<bool> isEdgesExist;
    isEdgesExist.assign(this->verticesCount, false);
    int componentsWithEdgeCount = 0;
    for (auto edge : listOfEdges) {
        if (dsu.find(edge.first.first) != dsu.find(edge.first.second)) {
            dsu.unite(dsu.find(edge.first.first), dsu.find(edge.first.second));
            isEdgesExist[dsu.find(edge.first.first)] = true;
        }
    }
    for (int i = 0; i < verticesCount; ++i) {
        if (isEdgesExist[dsu.find(i)] == true) {
            beginVertex = dsu.find(i);
            componentsWithEdgeCount++;
        }
    }
    return componentsWithEdgeCount;
}*/

/*int ListOfEdges::getOddDegreeVerticesCount(int& beginVertex) const {
    vector<bool> isOddDegree;
    isOddDegree.assign(this->verticesCount, false);
    int oddDegreeCount = 0;
    for (auto edge : listOfEdges) {
        if (isOddDegree[edge.first.first] = !isOddDegree[edge.first.first]) {
            oddDegreeCount++;
        } else {
            oddDegreeCount--;
        }
        if (!directed) {
            if (isOddDegree[edge.first.second] = !isOddDegree[edge.first.second]) {
                oddDegreeCount++;
            } else {
                oddDegreeCount--;
            }
        }
    }
    beginVertex = -1;
    if (oddDegreeCount == 0 || oddDegreeCount > 2) {
        return oddDegreeCount;
    }
    for (int i = 0; i < this->verticesCount; ++i) {
        if (isOddDegree[i] = true) {
            beginVertex = i;
            return oddDegreeCount;
        }
    }
}*/
