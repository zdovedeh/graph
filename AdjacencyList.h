//
// Created by zdove on 14.02.2019.
//

#ifndef LAB1_ADJACENCYLIST_H
#define LAB1_ADJACENCYLIST_H


#include "Graph.h"

#include <vector>
#include <map>

using std::vector;
using std::map;

class AdjacencyList : public BaseGraph {
private:
    vector<map<int,int>> adjacencyList; // multygraph

private:
    //int getOddDegreeVerticesCount(int& beginVertex) const;

    //int getComponentsWithEdgesCount(int& beginVertex) const;

    //bool isBrige(pair<int, int> edge) const;

public:
    AdjacencyList();

    explicit AdjacencyList(int N, bool isDirected = false, bool isWeighted = false);

    ~AdjacencyList();

    BaseGraph *getSpaingTreePrima() const override;

    BaseGraph *getSpaingTreeBoruvka() const override;

    BaseGraph *getSpaingTreeKruscal() const override;

    BaseGraph *readGraph(string fileName) override;

    void addEdge(int from, int to, int weight) override;

    void removeEdge(int from, int to) override;

    int changeEdge(int from, int to, int newWeight) override;

    BaseGraph * transformToAdjacencyMatrix() override;

    BaseGraph * transformToListOfEdges() override;

    //int checkEuler(bool &circleExist) const override;
    BaseGraph *transformToAdjacencyList() override;

    //vector<int> getEuleranTourFleri() const override;

    void writeGraph(string fileName) const override;

    //BaseGraph * getSpaingTreePrima() const override;
};


#endif //LAB1_ADJACENCYLIST_H
