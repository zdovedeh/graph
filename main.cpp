#include "Graph.h"

#include <chrono>
#include <iostream>

using namespace std;

int main()
{
    chrono::high_resolution_clock::time_point t1;
    chrono::high_resolution_clock::time_point t2;

    Graph g;
    //g.readGraph("input1.txt");
    //g.readGraph("input_1e3_1e5.txt");
    g.readGraph("input_1e4_1e5.txt");
    //g.readGraph("input_1e5_1e5.txt");
    g.writeGraph("output.txt");

    t1 = chrono::high_resolution_clock::now();
    //g.transformToAdjMatrix();
    t2 = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count() << endl;

    //g.writeGraph("output1.txt");
    //g.readGraph("output1.txt");

    t1 = chrono::high_resolution_clock::now();
    //g.transformToAdjList();
    t2 = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count() << endl;

    //g.writeGraph("output2.txt");
    //g.readGraph("output2.txt");

    t1 = chrono::high_resolution_clock::now();
    //g.transformToListOfEdges();
    t2 = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count() << endl;

    //g.writeGraph("output3.txt");

    t1 = chrono::high_resolution_clock::now();
    Graph g1 = g.getSpaingTreePrima();
    t2 = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count() << endl;

    t1 = chrono::high_resolution_clock::now();
    Graph g2 = g.getSpaingTreeKruscal();
    t2 = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count() << endl;

    t1 = chrono::high_resolution_clock::now();
    Graph g3 = g.getSpaingTreeBoruvka();
    t2 = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count() << endl;

    g1.writeGraph("output1.txt");
    g2.writeGraph("output2.txt");
    g3.writeGraph("output3.txt");

    return 0;
}