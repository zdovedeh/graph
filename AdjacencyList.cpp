//
// Created by zdove on 14.02.2019.
//

#include "AdjacencyMatrix.h"
#include "AdjacencyList.h"
#include "ListOfEdges.h"
#include "DSU.h"

#include <sstream>
#include <set>

using std::ios;
using std::endl;
using std::make_pair;
using std::stringstream;
using std::set;

AdjacencyList::AdjacencyList() {
    this->verticesCount = 0;
    this->edgesCount = 0;
    this->directed = false;
    this->weighted = false;
}

AdjacencyList::AdjacencyList(int N, bool isDirected, bool isWeighted) {
    this->verticesCount = N;
    this->edgesCount = 0;
    this->directed = isDirected;
    this->weighted = isWeighted;
    this->adjacencyList.resize(N);
}

AdjacencyList::~AdjacencyList() {
    for (int i = 0; i < this->verticesCount; ++i) {
        for (auto it = this->adjacencyList[i].begin(); it != this->adjacencyList[i].end(); ) {
            this->removeEdge(i, (it++)->first);
        }
    }
    this->adjacencyList.clear();
}

BaseGraph *AdjacencyList::readGraph(string fileName) {
    fstream fin;
    stringstream ss;
    pair<int, int> connectedVertices;
    string line;
    char view;
    fin.open(fileName, ios::in);
    fin >> view >> this->verticesCount;
    fin >> this->directed >> this->weighted;
    getline(fin, line);
    this->adjacencyList.resize(this->verticesCount);
    for (int i = 0; i < this->verticesCount; ++i) {
        getline(fin, line);
        ss << line;
        while (ss >> connectedVertices.first) {
            if (this->weighted) {
                ss >> connectedVertices.second;
            } else {
                connectedVertices.second = 1;
            }
            this->addEdge(i, --connectedVertices.first, connectedVertices.second);
        }
        ss.clear();
    }
    fin.close();
    return this;
}

void AdjacencyList::addEdge(int from, int to, int weight) {
    if (!this->weighted && weight != 1) {
        return;
    }
    if (this->adjacencyList[from].insert(make_pair(to, weight)).second) {
        this->edgesCount++;
    }
    if (!this->directed) {
        this->adjacencyList[to].insert(make_pair(from, weight));
    }
}

void AdjacencyList::removeEdge(int from, int to) {
    if (this->adjacencyList[from].find(to) == this->adjacencyList[from].end()) {
        return;
    }
    this->edgesCount--;
    this->adjacencyList[from].erase(to);
    if (!this->directed) {
        this->adjacencyList[to].erase(from);
    }
}

int AdjacencyList::changeEdge(int from, int to, int newWeight) {
    int oldWeight = this->adjacencyList[from][to];
    if (!this->weighted) {
        return oldWeight;
    }
    this->adjacencyList[from][to] = newWeight;
    if (!this->directed) {
        this->adjacencyList[to][from] = newWeight;
    }
    return oldWeight;
}

BaseGraph * AdjacencyList::transformToAdjacencyMatrix() {
    BaseGraph* graph = new AdjacencyMatrix(this->verticesCount, this->directed, this->weighted);
    for (int i = 0; i < this->verticesCount; ++i) {
        for (auto it = this->adjacencyList[i].begin(); it != this->adjacencyList[i].end(); ) {
            if (this->directed || i <= it->first) {
                graph->addEdge(i, it->first, it->second);
                this->removeEdge(i, (it++)->first);
            } else {
                ++it;
            }
        }
    }
    return graph;
}

BaseGraph * AdjacencyList::transformToListOfEdges() {
    BaseGraph* graph = new ListOfEdges(this->verticesCount, this->directed, this->weighted);
    for (int i = 0; i < this->verticesCount; ++i) {
        for (auto it = this->adjacencyList[i].begin(); it != this->adjacencyList[i].end(); ) {
            if (this->directed || i <= it->first) {
                graph->addEdge(i, it->first, it->second);
                this->removeEdge(i, (it++)->first);
            } else {
                ++it;
            }
        }
    }
    return graph;
}

void AdjacencyList::writeGraph(string fileName) const {
    //Graph::writeGraph(fileName);
    fstream fout;
    //fout.open(fileName, ios::out | ios::app);
    fout.open(fileName, ios::out);
    fout << "L " << this->verticesCount << endl;
    fout << this->directed << " " << this->weighted << endl;
    for (int i = 0; i < this->verticesCount; ++i) {
        //fout << i + 1 << ":\t";
        int temp = this->adjacencyList[i].size();
        for (auto item : adjacencyList[i])
        {
            fout << item.first + 1 << " " << item.second;
            if (temp-- != 1) {
                fout << " ";
            }
        }
        fout << endl;
    }
    fout.close();
}

BaseGraph *AdjacencyList::transformToAdjacencyList() {
    return this;
}

BaseGraph *AdjacencyList::getSpaingTreeKruscal() const {
    return nullptr;
}

BaseGraph *AdjacencyList::getSpaingTreeBoruvka() const {
    return nullptr;
}

/*BaseGraph *AdjacencyList::getSpaingTreePrima() const {
    if (this->edgesCount == 0) {
        return new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    }
    BaseGraph* spaningTree = new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    vector<int> distances;
    vector<int> parents;
    vector<bool> visited;
    distances.assign(this->verticesCount, INF);
    parents.assign(this->verticesCount, 0);
    visited.assign(this->verticesCount, false);
    int v = 0, u;
    distances[v] = 0;
    visited[v] = true;
    for (int i = 0; i < this->verticesCount - 1; ++i) {
        for (auto item : adjacencyList[v]) {
            if (!visited[item.first] && distances[item.first] > item.second) {
                distances[item.first] = item.second;
                parents[item.first] = v;
                u = item.first;
            }
        }
        visited[u] = true;
        v = u;
        spaningTree->addEdge(parents[u], u, this->adjacencyList[parents[u]].at(u));
    }
    return spaningTree;
}*/

BaseGraph *AdjacencyList::getSpaingTreePrima() const {
    if (this->edgesCount == 0) {
        return new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    }
    BaseGraph* spaningTree = new AdjacencyList(this->verticesCount, this->directed, this->weighted);
    vector<int> distances;
    vector<int> parents;
    vector<bool> visited;
    set<pair<int, int>> verticesQueue;
    distances.assign(this->verticesCount, INF);
    parents.assign(this->verticesCount, 0);
    visited.assign(this->verticesCount, false);
    distances[0] = 0;
    for (int i = 0; i < this->verticesCount; ++i) {
        verticesQueue.insert(make_pair(distances[i], i));
    }
    int v = verticesQueue.begin()->second;
    verticesQueue.erase(verticesQueue.begin());
    visited[v] = true;
    while (!verticesQueue.empty()) {
        for (auto item : adjacencyList[v]) {
            if (!visited[item.first] && distances[item.first] > item.second) {
                verticesQueue.erase(make_pair(distances[item.first], item.first));
                distances[item.first] = item.second;
                parents[item.first] = v;
                verticesQueue.insert(make_pair(distances[item.first], item.first));
            }
        }
        v = verticesQueue.begin()->second;
        verticesQueue.erase(verticesQueue.begin());
        visited[v] = true;
        spaningTree->addEdge(parents[v], v, distances[v]);
    }

    return spaningTree;
}

/*int AdjacencyList::getOddDegreeVerticesCount(int &beginVertex) const {
    vector<bool> isOddDegree;
    isOddDegree.assign(this->verticesCount, false);
    int oddDegreeCount = 0;
    for (int i = 0; i < this->verticesCount; ++i) {
        for (auto item : this->adjacencyList[i]) {
            if (isOddDegree[i] = !isOddDegree[i]) {
                oddDegreeCount++;
            } else {
                oddDegreeCount--;
            }
            if (!directed) {
                if (isOddDegree[item.first] = !isOddDegree[item.first]) {
                    oddDegreeCount++;
                } else {
                    oddDegreeCount--;
                }
            }
        }
    }
    beginVertex = -1;
    if (oddDegreeCount == 0 || oddDegreeCount > 2) {
        return oddDegreeCount;
    }
    for (int i = 0; i < this->verticesCount; ++i) {
        if (isOddDegree[i] = true) {
            beginVertex = i;
            return oddDegreeCount;
        }
    }
}*/

/*int AdjacencyList::getComponentsWithEdgesCount(int &beginVertex) const {
    DSU dsu = DSU(this->verticesCount);
    vector<bool> isEdgesExist;
    isEdgesExist.assign(this->verticesCount, false);
    int componentsWithEdgeCount = 0;
    for (int i = 0; i < this->verticesCount; ++i) {
        for (auto item : this->adjacencyList[i]) {
            if ((this->directed || i <= item.first) && dsu.find(i) != dsu.find(item.first)) {
                dsu.unite(dsu.find(i), dsu.find(item.first));
                isEdgesExist[dsu.find(i)] = true;
            }
        }
    }
    for (int i = 0; i < this->verticesCount; ++i) {
        if (isEdgesExist[dsu.find(i)] == true) {
            beginVertex = dsu.find(i);
            componentsWithEdgeCount++;
        }
    }
    return componentsWithEdgeCount;
}*/

/*int AdjacencyList::checkEuler(bool &circleExist) const {
    pair<int, int> beginVertex;
    int oddDegreeVerticesCount = getOddDegreeVerticesCount(beginVertex.first);
    int componentsWithEdgesCount = getComponentsWithEdgesCount(beginVertex.second);
    circleExist = oddDegreeVerticesCount <= 2 && componentsWithEdgesCount <= 1;
    if (oddDegreeVerticesCount == 0 && componentsWithEdgesCount <= 1) {
        circleExist = true;
        return beginVertex.second + 1;
    } else if (oddDegreeVerticesCount <= 2 && componentsWithEdgesCount <= 1) {
        circleExist = false;
        return beginVertex.first + 1;
    } else {
        circleExist = false;
        return 0;
    }
}*/

/*vector<int> AdjacencyList::getEuleranTourFleri() {
    bool circleExist;
    int beginVertex = checkEuler(circleExist) - 1;
    if (beginVertex == -1) {
        return vector<int>();
    }
    list<pair<int, int>> edges;
    for (int i = 0; i < this->verticesCount; ++i) {
        for (auto item : adjacencyList[i]) {
            edges.push_back(pair<int, int>(i, item.first));
        }
    }
    for (auto edge : edges) {
        if (isBrige(edge)) {
            pair<int, int> temp = edges.front();
            edges.pop_front();
            edges.push_back(temp);
        }
    }
    return Graph::getEuleranTourFleri();
}

bool AdjacencyList::isBrige(pair<int, int> edge) const {
    queue<int> queueVertices;
    vector<bool> visited = vector<bool>(this->verticesCount, false);
    queueVertices.push(edge.first);
    visited[edge.first] = true;
    int u;
    while (!queueVertices.empty()) {
        u = queueVertices.front();
        queueVertices.pop();
        for (auto item : adjacencyList[u]) {
            if (item.first == edge.second && u != edge.first) {
                return  true;
            } else if (item.first != edge.second) {
                queueVertices.push(item.first);
                visited[item.first] = true;
            }
        }
    }
    return false;
}*/
