//
// Created by zdove on 14.02.2019.
//

#ifndef LAB1_ADJACENCYMATRIX_H
#define LAB1_ADJACENCYMATRIX_H

#include "BaseGraph.h"

#include <vector>

using std::vector;

class AdjacencyMatrix : public BaseGraph {
private:
    vector<vector<int>> adjacencyMatrix; // not 0

public:
    AdjacencyMatrix();

    explicit AdjacencyMatrix(int N, bool isDirected = false, bool isWeighted = false);

    ~AdjacencyMatrix();

    BaseGraph *getSpaingTreePrima() const override;

    BaseGraph *getSpaingTreeBoruvka() const override;

    BaseGraph *getSpaingTreeKruscal() const override;

    BaseGraph* readGraph(string fileName) override;

    BaseGraph *transformToAdjacencyMatrix() override;

    void addEdge(int from, int to, int weight) override;

    void removeEdge(int from, int to) override;

    int changeEdge(int from, int to, int newWeight) override;

    BaseGraph * transformToAdjacencyList() override;

    BaseGraph * transformToListOfEdges() override;

    void writeGraph(string fileName) const override;
};


#endif //LAB1_ADJACENCYMATRIX_H
