//
// Created by zdove on 11.02.2019.
//

#ifndef LAB1_GRAPH_H
#define LAB1_GRAPH_H

#include "BaseGraph.h"

#include <fstream>
#include <vector>
#include <map>

using std::fstream;
using std::string;

class Graph {
private:
    BaseGraph* graph;
public:

protected:
    //bool isConnected() const;

public:
    Graph();

    explicit Graph(int N, bool isDirected = false, bool isWeighted = false);

    explicit Graph(BaseGraph* graph);

    ~Graph();

    void readGraph(string fileName);

    void addEdge(int from,int to, int weight);

    void removeEdge(int from,int to);

    int changeEdge(int from,int to,int newWeight);

    void transformToAdjList();

    void transformToAdjMatrix();

    void transformToListOfEdges();

    void writeGraph(string fileName) const;

    Graph getSpaingTreePrima();

    Graph getSpaingTreeKruscal();

    Graph getSpaingTreeBoruvka();

    //int checkEuler(bool& circleExist) const;

    //vector<int> getEuleranTourFleri() const;

    //vector<int> getEuleranTourEffective() const;
};


#endif //LAB1_GRAPH_H
