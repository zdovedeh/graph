//
// Created by zdove on 13.02.2019.
//

#ifndef LAB1_DSU_H
#define LAB1_DSU_H

#include <vector>

using std::vector;

class DSU {
private:
    vector<int> subsets;
public:
    DSU(int N);
    int find(int x);
    void unite(int x, int y);
};


#endif //LAB1_DSU_H
