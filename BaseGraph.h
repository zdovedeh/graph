//
// Created by zdove on 13.03.2019.
//

#ifndef LAB1_BASEGRAPH_H
#define LAB1_BASEGRAPH_H

#include <string>

using std::string;

class BaseGraph {
protected:
    const int INF = 1000000000;
    int verticesCount = 0;
    int edgesCount = 0;
    bool directed = false;
    bool weighted = false;

public:
    virtual BaseGraph* readGraph(string fileName) = 0;

    virtual void addEdge(int from,int to, int weight) = 0;

    virtual void removeEdge(int from,int to) = 0;

    virtual int changeEdge(int from,int to,int newWeight) = 0;

    virtual BaseGraph * transformToAdjacencyList() = 0;

    virtual BaseGraph * transformToAdjacencyMatrix() = 0;

    virtual BaseGraph * transformToListOfEdges() = 0;

    virtual void writeGraph(string fileName) const = 0;

    virtual BaseGraph * getSpaingTreePrima() const = 0;

    virtual BaseGraph * getSpaingTreeKruscal() const = 0;

    virtual BaseGraph * getSpaingTreeBoruvka() const = 0;

    //virtual int checkEuler(bool& circleExist) const = 0;

    //virtual vector<int> getEuleranTourFleri() const = 0;

    //virtual vector<int> getEuleranTourEffective() const = 0;
};


#endif //LAB1_BASEGRAPH_H
