//
// Created by zdove on 13.02.2019.
//

#include "DSU.h"

#include <cstdlib>

DSU::DSU(int N) {
    subsets = vector<int>(N);
    for (int i = 0; i < N; ++i) {
        subsets[i] = i;
    }
}

int DSU::find(int x) {
    if (x == subsets[x]) {
        return x;
    }
    return subsets[x] = find(subsets[x]);
}

void DSU::unite(int x, int y) {
    x = find(x);
    y = find(y);
    //if (x != y) {
    //    subsets[y] = x;
    //}
    if(rand() % 2 == 0) {
        subsets[x] = y;
    } else {
        subsets[y] = x;
    }
}
