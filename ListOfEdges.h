//
// Created by zdove on 14.02.2019.
//

#ifndef LAB1_LISTOFEDGES_H
#define LAB1_LISTOFEDGES_H


#include "Graph.h"

#include <map>

using std::map;
using std::pair;

class ListOfEdges : public BaseGraph {
private:
    map<pair<int,int>,int> listOfEdges;

private:
    //int getOddDegreeVerticesCount(int& beginVertex) const;

    //int getComponentsWithEdgesCount(int& beginVertex) const;

public:
    ListOfEdges();

    explicit ListOfEdges(int N, bool isDirected = false, bool isWeighted = false);

    BaseGraph *transformToListOfEdges() override;

    ~ListOfEdges();

    BaseGraph *readGraph(string fileName) override;

    void addEdge(int from, int to, int weight) override;

    void removeEdge(int from, int to) override;

    int changeEdge(int from, int to, int newWeight) override;

    BaseGraph * transformToAdjacencyList() override;

    BaseGraph *getSpaingTreePrima() const override;

    BaseGraph *getSpaingTreeBoruvka() const override;

    BaseGraph * getSpaingTreeKruscal() const override;

    BaseGraph * transformToAdjacencyMatrix() override;

    //int checkEuler(bool &circleExist) const override;

    //BaseGraph * getSpaingTreeBoruvka() const override;

    void writeGraph(string fileName) const override;
};


#endif //LAB1_LISTOFEDGES_H
